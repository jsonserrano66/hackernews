
var socket = false;
var me = this;
var findings = [];
var keywords = {};
var excludeids = [];
var excludekeywords = [];
var activeItem=null;

function openSocket() {
    if(socket!==false) return;
    try {
        socket = new WebSocket("wss://192.168.1.6/ws");
    } catch(e) {
        window.setTimeout("openSocket()", 3000);
    }
    socket.onmessage = function(e){
        setTimeout(function() { me.messageHandler(e); }, 10);
    }
    socket.onclose = function() {
        socket = false;
    }
}

function messageHandler(e) {
    try {
        data = JSON.parse(e.data);
        if(typeof data.type !== 'undefined' && typeof data.payload !== 'undefined'){
            if(data.type=="item"){
                addItems([data.payload]);
            } else if(data.type=="keywords"){
                keywords=data.payload;
            } else if(data.type=="exclusions"){
                if(data.payload.ids) excludeids=excludeids.concat(data.payload.ids);
                if(data.payload.keywords) excludekeywords=excludekeywords.concat(data.payload.keywords);
            } else if(data.type=="keywordconfirmation"){
                excludekeywords.push(data.payload);
                if(activeItem!=null) openItem(activeItem);
                alert("The keyword "+data.payload+" was successfully excluded.");
            } else if(data.type=="dismissconfirmation"){
                clearItem();
                document.getElementById("itemlist").removeChild(document.getElementById(data.payload));
            } else if(data.type=="error"){
                alert(data.payload);
            }
        }
    } catch(ex) {
    }
}

function generateItem(item){
    text="<div class=\"item\""+(item.id?" id=\""+item.id+"\"":"")+" onClick=\"openItem("+(item.id?item.id:"")+")\">\n";
	text+="\t<div class=\"itemheader\">";
	text+="\t\t<div class=\"itemscore\""+(item.scoreA?" style=\"background-color:rgb("+getColor(item.scoreA)+");\"":"")+">"+(item.scoreA?item.scoreA:"")+"</div>\n";
	text+="\t\t\t<div class=\"itemtitle\">"+getTitle(item)+"</div>\n";
	text+="\t\t\t<div class=\"clear\"></div>\n";
	text+="\t\t</div>\n";
	text+="\t<div class=\"itembody\">"+(item.text?cleanText(item.text):"")+"</div>\n";
    text+="</div>\n";
    return text;
}

function getTitle(item){
    title="";
    while(item.parent){
        item=item.parent;
    }
    if(item.title){
        title=item.title;
    }
    return title;
}

function cleanText(text){
    text=text.replace("</a>","");
    while(text.indexOf("<a")>=0){
        text=text.substring(0,text.indexOf("<a"))+text.substring(text.indexOf(">",text.indexOf("<a"))+1,text.length);
    }
    return text;
}

function getColor(score){
    if(score<=200) return "192,192,96";
    else if(score>=400) return "192,96,96";
    else return "192,"+(192-96*(score-200)/200)+",96";
}

function clearItem(){
    activeItem=null;
    document.getElementById("detailtitle").innerHTML="";
    document.getElementById("detailmessages").innerHTML="";
}

function openItem(id){
    item=null;
    for(i=0;item==null && i<findings.length;i++){
        if(findings[i].id && findings[i].id==id) item=findings[i];
    }
    if(item==null) {
        alert("invalid item");
        activeItem=null;
        return;
    }

    activeItem=id;
    text="";
    endtext="";
    title="";
    while(item){
        if(item.text){
            highlights=highlightText(item.text);
            if(highlights[0]>0){
                text="<div class=\"message\">\n\t<div class=\"messagetext\" onClick=\"window.open('https://news.ycombinator.com/item?id="+item.id+"','_blank')\">"+highlights[1]+"</div>\n"+text;
                endtext+="</div>\n";
            }
        }
        if(item.title) title=item.title;
        item=item.parent;
    }
    document.getElementById("detailmessages").innerHTML="";
    document.getElementById("detailtitle").innerHTML=title;
    document.getElementById("detailtitle").onclick=function(){
        window.open("https://news.ycombinator.com/item?id="+activeItem,"_blank");
    };
    document.getElementById("detailmessages").insertAdjacentHTML("beforeend",text+endtext);
}

function highlightText(text) {
    lowertext=text.toLowerCase();
    count=0;
    for (const key of Object.keys(keywords)) {
        pointer=(lowertext.indexOf(key)>=0?lowertext.indexOf(key):lowertext.length);
        while(pointer<lowertext.length){
            //check if excludekeyword, if so, skip it
            exfound=false;
            for(i=0;!exfound && i<excludekeywords.length;i++){
                offset=0;
                while(!exfound && offset+key.length<=excludekeywords[i].length){
                    if(lowertext.substring(pointer-offset,(pointer-offset)+excludekeywords[i].length)==excludekeywords[i]){
                        exfound=true;
                    }
                    offset++;
                }
            }

            //check if inside an HTML tab
            if(lowertext.substring(0,pointer).lastIndexOf("<")>=0 && lowertext.substring(0,pointer).lastIndexOf("<")>lowertext.substring(0,pointer).lastIndexOf(">") && lowertext.substring(pointer+key.length,lowertext.length).indexOf(">")>=0 && lowertext.substring(pointer+key.length,lowertext.length).indexOf(">")<lowertext.substring(pointer+key.length,lowertext.length).indexOf("<")){
                exfound=true;                
            }

            
            if(exfound){
                pointer+=key.length;
            } else {
                //continue on, injecting our tags
                count++;
                start=pointer;
                end=pointer+key.length;
                endcharacters=[" ",".","!",",","?","\/",":","\\","$","&",";","@","#","%","^","*","(",")","[","]","{","}","<",">"];

                while(start>0 && !endcharacters.includes(lowertext.substring(start-1,start))){
                    start--;
                }
                while((end+1)<lowertext.length && !endcharacters.includes(lowertext.substring(end,end+1))){
                    end++;
                }

                if(start==pointer && end==pointer+key.length){
                    oldlength=text.length;
                    text = text.substring(0,start)+"<span class=\"keyword\">"+text.substring(start,end)+"</span>"+text.substring(end,text.length);
                    lowertext=text.toLowerCase();
                    pointer+=(text.length-oldlength);
                } else {
                    oldlength=text.length;
                    text = text.substring(0,start)+"<span class=\"wholeword\" onClick=\"excludeWord(event,'"+text.substring(start,end).toLowerCase()+"');\">"+text.substring(start,pointer)+"<span class=\"keyword\">"+text.substring(pointer,pointer+key.length)+"</span>"+text.substring(pointer+key.length,end)+"</span>"+text.substring(end,text.length);
                    lowertext=text.toLowerCase();
                    pointer+=(end-start)+60;
                    pointer+=(text.length-oldlength);
                }
            }
            pointer=(lowertext.indexOf(key,pointer)>=0?lowertext.indexOf(key,pointer):lowertext.length);
        }
    }
    return [count,text];
}

function excludeWord(e,word){
    e.preventDefault();
    sure=confirm("Are you sure you want to exclude "+word+"?");
    if(sure){
        if(socket==false) openSocket();
        try {
            socket.send(JSON.stringify({action:"excludekeyword",payload:word}));
        } catch(e) {
            alert("Websocket error excluding the keyword.");
            window.setTimeout("openSocket()", 3000);
        }
    }
    return false;
}

function dismiss() {
    if(activeItem!=null) dismissId(activeItem);
}

function dismissId(id){
    if(socket==false) openSocket();
    try {
        socket.send(JSON.stringify({action:"dismissid",payload:id}));
    } catch(e) {
        alert("Websocket error dismissing the item.");
        window.setTimeout("openSocket()", 3000);
    }
}

function loadJSON(callback) {
    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', 'findings.json', true);
    xobj.onreadystatechange = function () {
        if (xobj.readyState == 4 && xobj.status == "200") {
            text=xobj.responseText;
            if(text.length==0) text="[";
            text+="]";
            callback(JSON.parse(text));
        }
    };
    xobj.send();
}

var addItems = function(items) {
    findings = findings.concat(items)
    for (const item of items) {
        document.getElementById("itemlist").insertAdjacentHTML("beforeend",generateItem(item))
    }
}